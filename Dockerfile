# Use an official OpenJDK runtime as a parent image
FROM openjdk:11-jre-slim

# Set the working directory in the container
WORKDIR /app

# Copy the packaged jar file into the container at /app
COPY target/my-java-app-1.0-SNAPSHOT.jar /app/my-java-app.jar

# Run the jar file
ENTRYPOINT ["java", "-jar", "my-java-app.jar"]
